# Первая лекция

09/12/2000

## Структура Орбиты

Ком. состав:

- Даша - коммандир
- Илья (Наруто) - коммисар
- Катя - методист
- Вика - комендант

МООО "РСО" крупнейшая молодежная организация страны

25.12.20014 - День рождения Орбиты
С 2017 года входит в СПБСО

## Регламент занятий

1. Необходимо поестить ВСЕ занятия (или сдать долг по пропущенному занятию на личной встрече с Катей)
2. За 3 пропуска без уважительной причины исключают из отряда
3. Необходимо сдать все тесты. Всего дается три попытки, но каждый раз порог для сдачи увеличивается. Самый сложный тест - через полтора месяца (где-то в середине января)
4. Всем нужно пройти 2 пратики:
   - либо меро для школы (5-8 класс)
   - либо что-то ещё похожее
5. Будет методический выезд в тот самый лагерь, где нас испытвют на прочность только без детей
6. Папка вожатого:
   - игры
     - на знакомство
     - сплочение
     - удовольствие
     - выявление лидера
     - с залом
     - ролевые
   - отрядные меро
     - свечки
     - лагерные песни
     - загадки
     - легенды (типо сказок)
7. Должны быть сданы все зачеты. Практический материал, написание своих игр и т.п.

## Кодекс вожатого

| #   | Обязательно льзя                                                                                                     | Нельзя никогда                                                                                                            |
| --- | -------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------- |
| 1   | Всегда знать, где находятся дети                                                                                     | Наказывать детей физически (даже немного, например - тянуть за рукав в строй)                                             |
| 2   | Нести ответственность за жизнь детей                                                                                 | Показывать негативные эмоции                                                                                              |
| 3   | Следить за распорядком дня                                                                                           | Ссориться при детях                                                                                                       |
| 4   | Организовывать досуг детей (в т.ч. самостоятельно)                                                                   | Пить, курить, материться                                                                                                  |
| 5   | Найти общий язык с напарником                                                                                        | Лежать в кровати у детей                                                                                                  |
| 6   | Следить за гигиеной детей (и за ушами)                                                                               | Иметь интимные связи (держать дистанцию с детьми, потому что они могут все не так понять) (но конечно им нужны обнимашки) |
| 7   | Изучать особенности детей (физические, медицинские, психологические, характер, семейные особенности)                 | Кричать на детей                                                                                                          |
| 8   | Знать всех детей по именам (**к концу первого дня** (весь день спрашивать как их зовут))                             | Отпускать детей за территорию                                                                                             |
| 9   | Сплачивать команду                                                                                                   | Просить сладости у детей (но раз они угощааают)                                                                           |
| 10  | Следить за приемом пищи (узнать почему есть не хочет (вдруг у него нычка есть) и попросить съесть хотяяя бы 3 ложки) | Давать невыполнимых обещаний (и вестись на детей)                                                                         |
| 11  | Всегда иметь план Б                                                                                                  | Выделять любимчиков и отщепенцев                                                                                          |
| 12  | Давать положитльный пример                                                                                           | Не нарушать покой во время сна (тихий час по расписанию)                                                                  |

## Единые педагогические требования (ЕПТ)

Это система норм и правил, основанных на единых теоретических и методических подходах, и принятых всеми участниками педагогического процесса к неукоснительному исполнению.

(формулировку выучить)

## Игры

### Суета сует (на знакомство)

Всем участникам раздаются карточки, которые разделены на 9-16 клеточек. В каждой клеточке записано задание. Суть одна: записать имя человека, который - ловит рыбу, и держит дома собаку, любит мечтать под звездами и т.д. Или же записать туда какие-нибудь качества (самый улыбчивый, голубоглазый и т.п.). Чем неожиданней будет задание, тем лучше. Можно заложить в эту карточку то, что вам нужно, например, выявить любителей рисования, пения, игры на гитаре и т.д. Побеждает тот, кто быстрее и точнее соберёт имена.

### Молекула (на знакомство)

Все участники хаотично ходят по площадке и, как только ведущий говорит "Молекула - {число}", дети должны объединиться в группы по {число} человек и узнать имена друг друга. В это время напарник ходит по образовавшимся молекулам и просит детей назвать имена новых знакомых. Затем по команде ведущего образуются новые молекулы уже из большего количества детей. Продолжать можно до тех пор пока не образуются две большие молекулы (ну или смотреть по ситуации)
